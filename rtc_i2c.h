#ifndef RTC_I2C_H_
#define RTC_I2C_H_

#include "i2c_driver.h"
#include "driverlib/timer.h"
#include "driverlib/uart.h"
#include "utils/uartstdio.h"

//*****************************************************************************
//
// Function prototype.
//
//*****************************************************************************
void init_uart(void);
void init_timer(void);

void set_rtc(uint8_t year,
             uint8_t month,
             uint8_t date,
             uint8_t day,
             uint8_t hour,
             uint8_t min,
             uint8_t sec);

void read_rtc(void);

uint8_t dec2bcd(uint8_t dec);
uint8_t bcd2dec(uint8_t bcd);

#endif /* RTC_I2C_H_ */
