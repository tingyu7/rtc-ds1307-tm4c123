#include "rtc_i2c.h"

//*****************************************************************************
//
// Signal for timer timeout interrupt.
//
//*****************************************************************************
static uint8_t tick = 0;

//*****************************************************************************
//
// The UART initialization for console printing
//
//*****************************************************************************
void init_uart(void) {
    // Enable the GPIO Peripheral used by the UART.
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);

    // Wait for the Peripheral to be ready for programming
    while (!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOA));

    // Enable UART0
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);

    // Wait for the Peripheral to be ready for programming
    while (!SysCtlPeripheralReady(SYSCTL_PERIPH_UART0));

    // Configure GPIO Pins for UART mode.
    GPIOPinConfigure(GPIO_PA0_U0RX);
    GPIOPinConfigure(GPIO_PA1_U0TX);
    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

    // Use the internal 16MHz oscillator as the UART clock source.
    UARTClockSourceSet(UART0_BASE, UART_CLOCK_PIOSC);

    // Initialize the UART for console I/O.
    UARTStdioConfig(0, 9600, 16000000);
}

//*****************************************************************************
//
// The Timer interrupt initialization.
//
//*****************************************************************************
void init_timer(void) {
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);

    // Wait for the Peripheral to be ready for programming
    while (!SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER0));

    TimerConfigure(TIMER0_BASE, TIMER_CFG_PERIODIC);

    // Set timeout interrupt with freq = 1 Hz
    TimerLoadSet(TIMER0_BASE, TIMER_A, SysCtlClockGet()-1);

    IntEnable(INT_TIMER0A);
    TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
    IntMasterEnable();

    TimerEnable(TIMER0_BASE, TIMER_A);
}

//*****************************************************************************
//
// Conversion from DEC to BCD.
//
//*****************************************************************************
uint8_t dec2bcd(uint8_t dec) {
    return ((dec/10 * 16) + (dec % 10));
}

//*****************************************************************************
//
// Conversion from BCD to DEC.
//
//*****************************************************************************
uint8_t bcd2dec(uint8_t bcd) {
    return ((bcd/16 * 10) + (bcd % 16));
}

//*****************************************************************************
//
// Adjust the RTC.
//
//*****************************************************************************
void set_rtc(uint8_t year,
             uint8_t month,
             uint8_t date,
             uint8_t day,
             uint8_t hour,
             uint8_t min,
             uint8_t sec) {

    uint8_t adj_buf[7];
    adj_buf[0] = dec2bcd(sec);
    adj_buf[1] = dec2bcd(min);
    adj_buf[2] = dec2bcd(hour);
    adj_buf[3] = dec2bcd(day);
    adj_buf[4] = dec2bcd(date);
    adj_buf[5] = dec2bcd(month);
    adj_buf[6] = dec2bcd(year);

    UARTprintf("Adjusting RTC ...\n");
    i2c_transmit(adj_buf, RTC_I2C_ADDR, sizeof(adj_buf), 0x00);
    UARTprintf("RTC Adjustment Complete!\n");
}

//*****************************************************************************
//
// Read RTC.
//
//*****************************************************************************
void read_rtc(void) {
    uint8_t now_buf[7];
    UARTprintf("Reading Current Time...\n");

    i2c_receive(now_buf, RTC_I2C_ADDR, sizeof(now_buf), 0x00);

    UARTprintf("20\%02u/\%02u/\%02u\n", bcd2dec(now_buf[6]),
               bcd2dec(now_buf[5]), bcd2dec(now_buf[4]));
    UARTprintf("\%02u:\%02u:\%02u\n\n", bcd2dec(now_buf[2]),
               bcd2dec(now_buf[1]), bcd2dec(now_buf[0]));
}

//*****************************************************************************
//
// Main
//
//*****************************************************************************
void main() {

    // Set system clock to 80 MHz
    SysCtlClockSet(SYSCTL_SYSDIV_2_5 |
                   SYSCTL_USE_PLL |
                   SYSCTL_OSC_MAIN |
                   SYSCTL_XTAL_16MHZ);

    // Initialize peripherals
    init_uart();
    init_i2c();

//    set_rtc(18, 5, 4, 5, 14, 8, 0);

    // Set timer interrupt to trigger read rtc every second
    init_timer();

    while (1) {
        if (tick == 1) {
            tick = 0;
            read_rtc();
        }
    }
}

//*****************************************************************************
//
// Timer interrupt handler.
//
//*****************************************************************************
void TimerA0IntHandler(void) {
    // Clear the timer interrupt
    TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);

    tick = 1;
}
