#ifndef I2C_DRIVER_H_
#define I2C_DRIVER_H_

#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_i2c.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "driverlib/i2c.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/interrupt.h"
#include "inc/tm4c123gh6pm.h"

//*****************************************************************************
//
// FSM state encoding.
//
//*****************************************************************************
#define STATE_IDLE                 0
#define STATE_READ_ONE             1
#define STATE_READ_FIRST           2
#define STATE_READ_NEXT            3
#define STATE_READ_FINAL           4
#define STATE_READ_WAIT            5
#define STATE_WRITE_NEXT           6
#define STATE_WRITE_FINAL          7

//*****************************************************************************
//
// Slave address for RTC.
//
//*****************************************************************************
#define RTC_I2C_ADDR 0x68

//*****************************************************************************
//
// The variables that track the data to be transmitted or received.
//
//*****************************************************************************
extern uint8_t *g_buf;
extern uint32_t g_num_bytes;

//*****************************************************************************
//
// The current state of the i2c interrupt handler state machine.
//
//*****************************************************************************
extern volatile uint32_t g_state;

//*****************************************************************************
//
// Function prototype.
//
//*****************************************************************************
void init_i2c(void);

void i2c_transmit(uint8_t *buf,
                  uint8_t slave_addr,
                  uint8_t num_bytes,
                  uint8_t reg);

void i2c_receive(uint8_t *buf,
                 uint8_t slave_addr,
                 uint8_t num_bytes,
                 uint8_t reg);

void I2C0IntHandler(void);

#endif /* I2C_DRIVER_H_ */
