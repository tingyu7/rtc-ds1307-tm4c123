#include "i2c_driver.h"

uint8_t *g_buf = 0;
uint32_t g_num_bytes = 0;
volatile uint32_t g_state = STATE_IDLE;

//*****************************************************************************
//
// The I2C initialization.
//
//*****************************************************************************
void init_i2c(void) {
    //enable GPIO peripheral that contains I2C 0
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);

    // Wait for the Peripheral to be ready for programming
    while (!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOB));

    // Configure the pin muxing for I2C0 functions on port B2 and B3.
    GPIOPinConfigure(GPIO_PB2_I2C0SCL);
    GPIOPinConfigure(GPIO_PB3_I2C0SDA);

    // Select the I2C function for these pins.
    GPIOPinTypeI2CSCL(GPIO_PORTB_BASE, GPIO_PIN_2);
    GPIOPinTypeI2C(GPIO_PORTB_BASE, GPIO_PIN_3);

    // Enable i2c module
    SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C0);

    // Wait for the Peripheral to be ready for programming
    while (!SysCtlPeripheralReady(SYSCTL_PERIPH_I2C0));

    // Enable and initialize the I2C0 master module.  Use the system clock for
    // the I2C0 module.  The last parameter sets the I2C data transfer rate.
    // If false the data rate is set to 100kbps and if true the data rate will
    // be set to 400kbps.
    I2CMasterInitExpClk(I2C0_BASE, SysCtlClockGet(), false);

    // Enable the I2C master interrupt.
    // Interrupt triggers when a transaction completes
    I2CMasterIntEnableEx(I2C0_BASE, I2C_MASTER_INT_DATA);

    // Enable the I2C interrupt.
    IntEnable(INT_I2C0);
}

//*****************************************************************************
//
// Write to the I2C device.
//
//*****************************************************************************
void i2c_transmit(uint8_t *buf,
                  uint8_t slave_addr,
                  uint8_t num_bytes,
                  uint8_t reg) {

    // Save buffer pointer and byte counter in global variable for
    // easier manipulation in the interrupt handler
    g_buf = buf;
    g_num_bytes = num_bytes;

    // Assign the next state of the state machine base on the number of
    // bytes to write
    if (num_bytes == 1) {
        g_state = STATE_WRITE_FINAL;
    } else {
        g_state = STATE_WRITE_NEXT;
    }

    // Specify that we are going to write to slave device to select reg to read
    I2CMasterSlaveAddrSet(I2C0_BASE, slave_addr, false);

    // Specify register to be written
    I2CMasterDataPut(I2C0_BASE, reg);

    // Sends the start bit, slave address + w/r bit and the reg address
    I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_BURST_SEND_START);

    // Wait until the I2C interrupt state machine is idle.
    while (g_state != STATE_IDLE);
}

//*****************************************************************************
//
// Read from the I2C device.
//
//*****************************************************************************
void i2c_receive(uint8_t *buf,
                 uint8_t slave_addr,
                 uint8_t num_bytes,
                 uint8_t reg) {

    // Save buffer pointer and byte counter in global variable for
    // easier manipulation in the interrupt handler
    g_buf = buf;
    g_num_bytes = num_bytes;

    // Assign the next state of the state machine base on the number of
    // bytes to read
    if (num_bytes == 1) {
        g_state = STATE_READ_ONE;
    } else {
        g_state = STATE_READ_FIRST;
    }

    // Specify that we are going to write to slave device to select reg to read
    I2CMasterSlaveAddrSet(I2C0_BASE, slave_addr, false);

    // Specify register to be read
    I2CMasterDataPut(I2C0_BASE, reg);

    // Using I2C_MASTER_CMD_BURST_SEND_START so that we can perform repeated
    // start later
    // Sends the start bit, slave address + w/r bit and the reg address
    I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_BURST_SEND_START);

    // Wait until the I2C interrupt state machine is idle.
    while (g_state != STATE_IDLE);
}

//*****************************************************************************
//
// The I2C interrupt handler.
//
//*****************************************************************************
void I2C0IntHandler(void) {
    I2CMasterIntClear(I2C0_BASE);

    switch (g_state) {
        // Idle state
        case STATE_IDLE:
            // Do nothing
            break;

        // Single read
        case STATE_READ_ONE:
            // Specify that we are going to read from slave device
            I2CMasterSlaveAddrSet(I2C0_BASE, RTC_I2C_ADDR, true);

            // Initiate receive of data from the slave, repeated start here
            I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_SINGLE_RECEIVE);

            g_state = STATE_READ_WAIT;

            break;

        // Start of burst read
        case STATE_READ_FIRST:
            // Specify that we are going to read from slave device
            I2CMasterSlaveAddrSet(I2C0_BASE, RTC_I2C_ADDR, true);

            I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_BURST_RECEIVE_START);

            g_state = STATE_READ_NEXT;

            break;

        // During a burst read
        case STATE_READ_NEXT:
            // Enters here after end of transmission of the data byte
            *g_buf++ = I2CMasterDataGet(I2C0_BASE);
            g_num_bytes--;

            I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_BURST_RECEIVE_CONT);

            // State changes if there are two bytes left to read
            if (g_num_bytes == 2) {
                g_state = STATE_READ_FINAL;
            }

            break;

        // End of burst read
        case STATE_READ_FINAL:
            // Enters here after end of transmission of the SECOND last byte
            *g_buf++ = I2CMasterDataGet(I2C0_BASE);
            g_num_bytes--;

            I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_BURST_RECEIVE_FINISH);

            g_state = STATE_READ_WAIT;

            break;

        // Read of a single read
        // or final read for a burst read
        case STATE_READ_WAIT:
            // Enters here after end of transmission of the LAST byte
            *g_buf = I2CMasterDataGet(I2C0_BASE);

            g_state = STATE_IDLE;

            break;

        // During a burst write
        case STATE_WRITE_NEXT:
            I2CMasterDataPut(I2C0_BASE, *g_buf++);
            g_num_bytes--;

            I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_BURST_SEND_CONT);

            if (g_num_bytes == 1) {
                g_state = STATE_WRITE_FINAL;
            }

            break;

        // Single write will enter here directly
        // or end of burst write
        case STATE_WRITE_FINAL:
            I2CMasterDataPut(I2C0_BASE, *g_buf);

            I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_BURST_SEND_FINISH);

            g_state = STATE_IDLE;

            break;
    }
}
